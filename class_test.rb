class CropRatio

    def initialize
      @total_weight = 0
      @crops = {}
    end
  
    def add(name, crop_weight)
      current_crop_weight = crop_weight
      if @crops[name]
        @crops[name] += current_crop_weight
      else
        @crops[name] = current_crop_weight
      end
      #puts @crops
      @total_weight += current_crop_weight
      #puts @total_weight
    end
  
    def proportion(name)
      #puts "Total of #{name} is #{@crops[name]}"
      #puts "total crop weight #{@total_weight}"
      result = (@crops[name]) / (@total_weight).to_f
    end
  
    
  end
  
  crop_ratio = CropRatio.new
  crop_ratio.add('Wheat', 4);
  crop_ratio.add('Wheat', 5);
  crop_ratio.add('Rice', 1);
  #crop_ratio.add('Rice', 2);

  #puts crop_ratio.proportion("Wheat")
  puts crop_ratio.proportion("Rice")
