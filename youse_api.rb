class AlbumsController < ActionController::Base
  before_action :set_album, only: [:show, :update, :destroy]
  def create
    # TODO
    @album = Album.create!(album_params)
    render json: @album, status: :ok
  end

  def show
    # TODO
    render json: @album, status: :ok
  end

  def index
    # TODO
    render json: Album.all, status: :ok
  end

  def update
    # TODO
    render json: @album, status: :ok if @album.update(album_params)

  end

  def destroy
    # TODO
    @album.destroy
    head :no_content, status: 204
  end

  private
  def album_params
    params.require(:data).permit(:title, :performer, :cost)
  end

  def set_album
    @album = Album.find(params[:id])
  end
end

class PurchasesController < ActionController::Base
  def create
    # TODO
    @purchase = Purchase.create!(purchase_params)
    # @purchase.users.total_purchases += 1
    @purchase.album.last_purchased_by = @purchase.user.name
    render json: @purchase, status: :ok
  end

  private
  def purchase_params
    params.require(:data).permit(:user_id, :album_id)
  end
end

ActiveRecord::Schema.define do
  # TODO: define schema
  create_table :albums do |t|
    t.column :title, :string
    t.column :performer, :string
    t.column :cost, :integer
    t.column :last_purchased_at, :datetime, default: nil
    t.column :last_purchased_by, :string, default: nil
  end

  create_table :purchases do |t|
    t.column :album_id, :integer
    t.column :user_id, :integer
    t.column :created_at, :datetime
  end

  create_table :users, force: :cascade do |t|
    t.column :name, :string
    t.column :total_purchases, :integer, default: 0
  end
end

class Album < ActiveRecord::Base
  # TODO: define Album model
  has_many :purchases
  validates :title, presence: true
  validates :performer, presence: true
  validates :cost, presence: true, numericality: { only_integer: true, gt: 0}
end

class Purchase < ActiveRecord::Base
  # TODO: define Purchase model
  belongs_to :user
  belongs_to :album
  validates_presence_of :album_id, :user_id
end

class User < ActiveRecord::Base
  # TODO: define User model
  has_many :purchases
  validates :name, presence: true
end
