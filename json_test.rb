require 'json'

def sort_by_price_ascending(json_string)
    array_hashes = JSON.parse(json_string)
    #puts "array of hashes sorted", arr_of_hashes, 
    arr_prices = array_hashes.map{|h| h['price']}
    # puts 'Array of prices', arr_prices
    # puts 'Array of prices unique', arr_prices.uniq

    if arr_prices.uniq.size != arr_prices.size
        return (array_hashes.sort_by {|h| h['name']}).to_json
    end
    return (array_hashes.sort_by {|h| h['price']}).to_json
        
    #return arr_of_hashes.to_json
end
  
  puts sort_by_price_ascending('[{"name":"eggs","price":9.99},{"name":"coffee","price":9.99},{"name":"rice","price":4.04}]')
  puts sort_by_price_ascending('[{"name":"eggs","price":9.98},{"name":"coffee","price":9.99},{"name":"rice","price":4.04}]')
