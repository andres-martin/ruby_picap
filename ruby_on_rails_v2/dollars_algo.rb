class Bank

    def least_number_of_dollars(dollars, amount)
        if !dollars || dollars.empty? || !amount || amount <= 0
            raise ArgumentError
        end
        result = 0
        dollars.reverse.each do |dollar|
            result += amount / dollar
            amount = amount % dollar
        end
        raise ArgumentError if amount != 0
        return result
    end

end
  
b = Bank.new
puts b.least_number_of_dollars([1,2,5], 11)
puts b.least_number_of_dollars([], 3)
