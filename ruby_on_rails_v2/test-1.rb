class Calculator
    def calculate_sum(a, b)
        if !a || !b || a.empty? || b.empty?
            raise ArgumentError
        end
        return a.join.to_i + b.join.to_i
    end
  end
  
  c = Calculator.new
  puts c.calculate_sum([1,2,3], [2])
