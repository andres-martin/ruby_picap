class CategoryTree

    def initialize
        @tree = {}
    end

    def add_category(category, parent)
      if parent == nil
        @tree[category] = [] 
      else
        if @tree[parent].include? category
            raise ArgumentError, 'Category already added' 
        end   
        @tree[parent] << category if category   
      end
    end
  
    def get_children(category)
        if !@tree[category]
            raise ArgumentError, 'Parent does not exists'
        end
      return @tree[category]
    end
  
  end
  
  c = CategoryTree.new
  c.add_category('A', nil)
  c.add_category('B', 'A')
  c.add_category('C', 'A')
  #c.add_category('C', 'A')
  c.add_category(nil, 'A')
  puts (c.get_children('L') || []).join(',')
