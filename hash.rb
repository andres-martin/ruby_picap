def group_by_owners(files)
    new_hash = {}
    values = files.values
    values.map{|e| new_hash[e] = []}
    # files.each do |k, v|
    #   new_hash[v] << k
    # end
    # files.to_a.map{|e| new_hash[e[1]] << e[0]}    
    files.map{|k, v| new_hash[v] << k}
    return new_hash
end
  
  files = {
    'Input.txt' => 'Randy',
    'Code.py' => 'Stan',
    'Output.txt' => 'Randy'
  }
  puts group_by_owners(files)
## well done!


def is_palindrome(word)
  return word.downcase == word.reverse.downcase
end
  
puts is_palindrome('Deleveled')
