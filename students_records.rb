# @param [Array<Hash>] students
#   Array of student records (array of hashes)
# @param [Array<Lambda>] filters
#   Zero or more filters to apply - each filter is a lambda returning a boolean
#
# @return [Array<Hash>]
#   An array of students who pass ALL provided filters
def pipeline(*funcs)
    -> (arg) {
      # write your code here  
      aux = arg
      funcs.map{|func| aux = func.call(arg)}
      return aux
    }
end

def filter(students, *filters)
    # Write your code here
    fun = pipeline(*filters)
    array = []
    students.each do |student|
      array << student if fun.call(student)   
    end
    return array
end
  
  students = [
    { name: 'Thomas Edison', gpa: 3.45 },
    { name: 'Grace Hopper', gpa: 3.99 },
    { name: 'Leonardo DaVinci', gpa: 2.78 }
  ]
  
  honor_roll = ->(record) { record[:gpa] > 3.0 }
  honor_greater = ->(record) { record[:name] == 'Martin' }
  
  honor_roll_members = filter(students, honor_roll)
  puts honor_roll_members # it should print
  # {:name=>"Thomas Edison", :gpa=>3.45} 
  # {:name=>"Grace Hopper", :gpa=>3.99}
